package nl.robfaber.gateway.filters;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import nl.rofaber.metrics.OutcomeUtil;
import nl.rofaber.metrics.RequestMetricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
public class OutgoingRequestsGatewayFilterFactory extends
    AbstractGatewayFilterFactory<OutgoingRequestsGatewayFilterFactory.Config> {

  @Autowired
  private RequestMetricsService requestMetricsService;
  @Value("${spring.application.name}")
  private String applicationName;


  public OutgoingRequestsGatewayFilterFactory() {
    super(Config.class);
  }

  @Override
  public List<String> shortcutFieldOrder() {
    return Arrays.asList("serviceId");
  }

  @Override
  public GatewayFilter apply(Config config) {
    return (exchange, chain) -> {
      val url = exchange.getRequest().getPath().toString();
      log.info("Outgoing request {} to {}", url, config.getServiceId());
      return chain.filter(exchange)
          .then(Mono.fromRunnable(() -> {
            val outcome = OutcomeUtil.outcome(exchange.getResponse().getRawStatusCode());
            log.info("Outgoing request {} -> {}", config.getServiceId(), outcome);
            requestMetricsService.outgoingRequest(config.getServiceId(), url, outcome);
          }));
    };
  }

  @Data
  @NoArgsConstructor
  public static class Config {
    private String serviceId;
  }
}
