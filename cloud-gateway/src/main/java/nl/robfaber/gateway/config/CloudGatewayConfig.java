package nl.robfaber.gateway.config;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import lombok.extern.slf4j.Slf4j;
import nl.rofaber.metrics.annotations.EnableRequestMetrics;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableRequestMetrics
@Slf4j
public class CloudGatewayConfig {
  @Value("${spring.application.name}")
  private String applicationName;

  @Bean
  public MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() throws UnknownHostException {
    List<Tag> tags = Arrays.asList(
        Tag.of("application", applicationName),
        Tag.of("host", InetAddress.getLocalHost().getHostName())
    );

    return registry -> registry.config().commonTags(tags);
  }

//  @Bean
//  public GlobalFilter postGlobalFilter() {
//    return (exchange, chain) -> chain.filter(exchange)
//        .then(Mono.fromRunnable(() -> log.info("Global Post Filter executed")));
//  }

//  @Bean
//  public RouteLocator routeLocator(RouteLocatorBuilder builder) {
//    return builder.routes()
//      .route(r ->
//        r.path("/example/**")
//          .filters(f ->
//            f.stripPrefix(1).retry(3)
//          )
//          .uri("lb://experience-service")
//      )
//      .build();
//  }
}