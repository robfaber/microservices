package nl.rofaber.metrics;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RequestMetricsServiceTest {

  @Mock
  private MeterRegistry meterRegistry;

  @InjectMocks
  private RequestMetricsService requestMetricsService = new RequestMetricsService(100L);

  private AtomicInteger successCount = new AtomicInteger();
  private AtomicInteger clientErrorCount = new AtomicInteger();
  private AtomicInteger serverErrorCount = new AtomicInteger();

  public class TagsMatcher implements ArgumentMatcher<Iterable<Tag>> {

    private String outcome;

    public TagsMatcher(String outcome) {
      this.outcome = outcome;
    }

    @Override
    public boolean matches(Iterable<Tag> tags) {
      if (tags == null) {
        return false;
      }

      boolean result = false;
      for (Tag tag : tags) {
        if (tag.getKey().equals("outcome") && tag.getValue().equals(outcome)) {
          result = true;
        }
      }
      return result;
    }
  }

  @Test
  public void updateMetrics() {

    when(meterRegistry.gauge(anyString(), argThat(new TagsMatcher(RequestMetricsService.SUCCESS)), any(AtomicInteger.class))).thenReturn(successCount);
    when(meterRegistry.gauge(anyString(), argThat(new TagsMatcher(RequestMetricsService.CLIENT_ERROR)), any(AtomicInteger.class))).thenReturn(clientErrorCount);
    when(meterRegistry.gauge(anyString(), argThat(new TagsMatcher(RequestMetricsService.SERVER_ERROR)), any(AtomicInteger.class))).thenReturn(serverErrorCount);

    requestMetricsService.outgoingRequest("my-service", "my-request", RequestMetricsService.SUCCESS);
    requestMetricsService.outgoingRequest("my-service", "my-request", RequestMetricsService.CLIENT_ERROR);
    requestMetricsService.outgoingRequest("my-service", "my-request", RequestMetricsService.SERVER_ERROR);
    requestMetricsService.outgoingRequest("my-service", "my-request", RequestMetricsService.SUCCESS);
    requestMetricsService.outgoingRequest("my-service", "my-request", RequestMetricsService.SUCCESS);
    requestMetricsService.outgoingRequest("my-service", "my-request", RequestMetricsService.CLIENT_ERROR);

    //We cannot know the actual size here because the might be split over multiple timestamps
    //But it should be bigger than 0
    assertTrue(requestMetricsService.getMetricsPerTimestamp().size() > 0);
    //Because we havent updated the metrics yet the atomic values are zero now
    assertThat(requestMetricsService.getValues().size(), is(0));

    requestMetricsService.updateMetrics();
    //We still expect the metrics to contain value because the code above probably wont run for more than 100ms (the window size that is)
    assertTrue(requestMetricsService.getMetricsPerTimestamp().size() > 0);
    assertThat(requestMetricsService.getValues().size(), is(3));
    assertThat(requestMetricsService.getValues().get(new Metric("outgoing.requests", Tags.of(Tag.of("serviceId", "my-service"), Tag.of("outcome", RequestMetricsService.SUCCESS)))).get(), is(3));
    assertThat(requestMetricsService.getValues().get(new Metric("outgoing.requests", Tags.of(Tag.of("serviceId", "my-service"), Tag.of("outcome", RequestMetricsService.CLIENT_ERROR)))).get(), is(2));
    assertThat(requestMetricsService.getValues().get(new Metric("outgoing.requests", Tags.of(Tag.of("serviceId", "my-service"), Tag.of("outcome", RequestMetricsService.SERVER_ERROR)))).get(), is(1));

    try {
      Thread.sleep(100L);
    } catch (InterruptedException ignore) {
    }
    //All metrics should have slided out of the window and be removed
    requestMetricsService.updateMetrics();

    assertThat(requestMetricsService.getMetricsPerTimestamp().size(), is(0));
    //We still expect the meters to be there with value zero
    assertThat(requestMetricsService.getValues().size(), is(3));
    requestMetricsService.getValues().values().forEach(value -> assertTrue(value.get() == 0));

    requestMetricsService.outgoingRequest("my-service", "my-request", RequestMetricsService.SUCCESS);
    requestMetricsService.outgoingRequest("my-service", "my-request", RequestMetricsService.CLIENT_ERROR);
    requestMetricsService.outgoingRequest("my-service", "my-request", RequestMetricsService.SERVER_ERROR);
    requestMetricsService.updateMetrics();
    assertTrue(requestMetricsService.getMetricsPerTimestamp().size() > 0);
    assertThat(requestMetricsService.getValues().size(), is(3));
    requestMetricsService.getValues().values().forEach(value -> assertTrue(value.get() == 1));
  }

  @Test
  public void testMetricEquality1() {

    Metric metric1 = new Metric("outgoing.requests", Tags.of(Tag.of("serviceId", "service-A"), Tag.of("outcome", "SUCCESS")));
    Metric metric2 = new Metric("outgoing.requests", Tags.of(Tag.of("serviceId", "service-A"), Tag.of("outcome", "SUCCESS")));
    Metric metric3 = new Metric("outgoing.requests", Tags.of(Tag.of("serviceId", "service-A"), Tag.of("outcome", "CLIENT_ERROR")));
    Metric metric4 = new Metric("outgoing.requests", Tags.of(Tag.of("serviceId", "service-B"), Tag.of("outcome", "SUCCESS")));
    Metric metric5 = new Metric("incoming.requests", Tags.of(Tag.of("serviceId", "service-B"), Tag.of("outcome", "SUCCESS")));

    assertEquals(metric1, metric2);
    assertNotEquals(metric1, metric3);
    assertNotEquals(metric1, metric4);
    assertNotEquals(metric1, metric5);

    assertNotEquals(metric4, metric5);
  }

  @Test
  public void testMetricEquality2() {

    Metric metric1 = new Metric("outgoing.requests", Tags.of(Tag.of("serviceId", "service-A"), Tag.of("outcome", RequestMetricsService.SUCCESS)));
    Metric metric2 = new Metric("outgoing.requests", Tags.of(Tag.of("serviceId", "service-A"), Tag.of("outcome", RequestMetricsService.SUCCESS)));
    Metric metric3 = new Metric("outgoing.requests", Tags.of(Tag.of("serviceId", "service-A"), Tag.of("outcome", RequestMetricsService.CLIENT_ERROR)));
    Metric metric4 = new Metric("outgoing.requests", Tags.of(Tag.of("serviceId", "service-B"), Tag.of("outcome", RequestMetricsService.SUCCESS)));
    Metric metric5 = new Metric("incoming.requests", Tags.of(Tag.of("serviceId", "service-B"), Tag.of("outcome", RequestMetricsService.SUCCESS)));

    assertEquals(metric1, metric2);
    assertNotEquals(metric1, metric3);
    assertNotEquals(metric1, metric4);
    assertNotEquals(metric1, metric5);

    assertNotEquals(metric4, metric5);
  }
}