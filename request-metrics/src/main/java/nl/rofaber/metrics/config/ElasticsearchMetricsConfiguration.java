package nl.rofaber.metrics.config;

import nl.rofaber.metrics.elasticsearch.ElasticSearchRestClientAspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "request-metrics.enabled", havingValue = "true")
public class ElasticsearchMetricsConfiguration {

  @Bean
  public ElasticSearchRestClientAspect elasticSearchRestClientAspect() {
    return new ElasticSearchRestClientAspect();
  }
}
