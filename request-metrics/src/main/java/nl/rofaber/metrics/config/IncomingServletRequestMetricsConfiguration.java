package nl.rofaber.metrics.config;

import nl.rofaber.metrics.http.incoming.IncomingServletRequestsMetricsFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "request-metrics.enabled", havingValue = "true")
public class IncomingServletRequestMetricsConfiguration {

  @Bean
  public IncomingServletRequestsMetricsFilter incomingServletRequestsMetricsFilter() {
    return new IncomingServletRequestsMetricsFilter();
  }
}
