package nl.rofaber.metrics.config;

import nl.rofaber.metrics.database.JpaMetricsAspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(value = "request-metrics.enabled", havingValue = "true")
public class JpaMetricsConfiguration {

  @Bean
  public JpaMetricsAspect jpaMetricsAspect() {
    return new JpaMetricsAspect();
  }
}
