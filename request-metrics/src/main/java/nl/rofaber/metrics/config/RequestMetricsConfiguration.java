package nl.rofaber.metrics.config;

import nl.rofaber.metrics.RequestMetricsService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@ConditionalOnProperty(value = "request-metrics.enabled", havingValue = "true")
public class RequestMetricsConfiguration {

  @Bean
  public RequestMetricsService requestMetricsService() {
    return new RequestMetricsService();
  }
}
