package nl.rofaber.metrics.epos;

import lombok.extern.slf4j.Slf4j;
import nl.rofaber.metrics.RequestMetricsService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class EposMetricsAspect {

  @Autowired
  private RequestMetricsService requestMetricsService;

  @Around("execution(public * eu.able.epos.*.*Service+.*(..))")
  public Object euAbleEposServiceCall(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    return handle(proceedingJoinPoint);
  }

  @Around("execution(public * nl.syntel.epos.*.*Service+.*(..))")
  public Object nlSyntelEposServiceCall(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    return handle(proceedingJoinPoint);
  }

  private Object handle(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();

    String className = methodSignature.getDeclaringType().getSimpleName();
    String methodName = methodSignature.getName();

    String request = String.format("%s->%s", className, methodName);

    try {
      Object result = proceedingJoinPoint.proceed();
      requestMetricsService.outgoingRequest("epos", request, RequestMetricsService.SUCCESS);
      return result;
    } catch (Throwable t) {
      requestMetricsService.outgoingRequest("epos", request, RequestMetricsService.SERVER_ERROR);
      throw t;
    }
  }

}
