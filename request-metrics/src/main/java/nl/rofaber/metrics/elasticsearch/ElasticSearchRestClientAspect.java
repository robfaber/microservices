package nl.rofaber.metrics.elasticsearch;

import lombok.extern.slf4j.Slf4j;
import nl.rofaber.metrics.RequestMetricsService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class ElasticSearchRestClientAspect {
  @Autowired
  private RequestMetricsService requestMetricsService;

  @Around("execution(* nl.ing.securities.elasticsearch.commons.AbstractElasticsearchRestClient+.*(..))")
  public Object restClientPerformRequest(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    return handle(proceedingJoinPoint);
  }

  private Object handle(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();

    String className = methodSignature.getDeclaringType().getSimpleName();
    String methodName = methodSignature.getName();

    String request = String.format("%s->%s", className, methodName);

    try {
      Object result = proceedingJoinPoint.proceed();
      requestMetricsService.outgoingRequest("elasticsearch", request, RequestMetricsService.SUCCESS);
      return result;
    } catch (Throwable t) {
      requestMetricsService.outgoingRequest("elasticsearch", request, RequestMetricsService.SERVER_ERROR);
      throw t;
    }
  }
}
