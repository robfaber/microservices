package nl.rofaber.metrics.http.outgoing;

import feign.InvocationHandlerFactory;
import feign.Target;
import feign.Util;
import nl.rofaber.metrics.RequestMetricsService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;

public class FeignInvocationHandler implements InvocationHandler {
  private final RequestMetricsService requestMetricsService;
  private final Target target;
  private final Map<Method, InvocationHandlerFactory.MethodHandler> dispatch;


  public FeignInvocationHandler(RequestMetricsService requestMetricsService, Target target, Map<Method, InvocationHandlerFactory.MethodHandler> dispatch) {
    this.requestMetricsService = requestMetricsService;
    this.target = Util.checkNotNull(target, "target", new Object[0]);
    this.dispatch = Util.checkNotNull(dispatch, "dispatch for %s", new Object[]{target});
  }

  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    if ("equals".equals(method.getName())) {
      try {
        Object otherHandler = args.length > 0 && args[0] != null ? Proxy.getInvocationHandler(args[0]) : null;
        return this.equals(otherHandler);
      } catch (IllegalArgumentException var5) {
        return false;
      }
    } else if ("hashCode".equals(method.getName())) {
      return this.hashCode();
    } else if ("toString".equals(method.getName())) {
      return this.toString();
    } else {
      return this.dispatch(method, args);
    }
  }

  public Object dispatch(Method method, Object[] args) throws Throwable {
    String serviceId = target.name();
    String url = target.url();
    try {
      Object result = this.dispatch.get(method).invoke(args);

      requestMetricsService.outgoingRequest(serviceId, url, RequestMetricsService.SUCCESS);

      return result;
    } catch (final Throwable e) {
      requestMetricsService.outgoingRequest(serviceId, url, RequestMetricsService.SERVER_ERROR);
      throw e;
    }
  }

  public boolean equals(Object obj) {
    if (obj instanceof FeignInvocationHandler) {
      FeignInvocationHandler other = (FeignInvocationHandler) obj;
      return this.target.equals(other.target);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return this.target.hashCode();
  }

  public String toString() {
    return this.target.toString();
  }
}