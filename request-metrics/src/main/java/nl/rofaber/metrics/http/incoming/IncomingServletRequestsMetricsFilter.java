package nl.rofaber.metrics.http.incoming;

import lombok.extern.slf4j.Slf4j;
import nl.rofaber.metrics.OutcomeUtil;
import nl.rofaber.metrics.RequestMetricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
@Slf4j
@Order(0)
public class IncomingServletRequestsMetricsFilter implements Filter {

  @Autowired
  private RequestMetricsService requestMetricsService;

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {

    HttpServletRequest httpRequest = (HttpServletRequest) request;
    String fromService = httpRequest.getHeader("X-from-service");

    //We don't track incoming requests from vizceral itself
    if ("vizceral-server".equalsIgnoreCase(fromService)) {
      filterChain.doFilter(request, response);
      return;
    }

    String url = httpRequest.getRequestURL().toString();
    try {
      filterChain.doFilter(request, response);
      String outcome = OutcomeUtil.outcome(((HttpServletResponse) response).getStatus());
      requestMetricsService.incomingRequest(fromService, url, outcome);
    } catch (ServletException | IOException | RuntimeException e) {
      requestMetricsService.incomingRequest(fromService, url, RequestMetricsService.SERVER_ERROR);
      throw e;
    }
  }
}
