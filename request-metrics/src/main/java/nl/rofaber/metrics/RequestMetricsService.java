package nl.rofaber.metrics;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The RequestMetricsService keeps track of incoming (for edge services only) and outgoing request metrics.
 * The metrics are the number of requests per minute;
 * 1. To this service (for incoming requests)
 * 2. From this service to another service (for outgoing requests)
 * <p>
 * The metrics have a certain name (for example URL which was called, database request) are tagged with;
 * 1. The outcome of the request (f.e. SUCCESS, CLIENT_ERROR, SERVER_ERROR)
 * 2. The service to/from the request was made
 * <p>
 * Metrics are published as gauges to the micrometer MeterRegistry on a scheduled basis.
 */
@Service
@Slf4j
public class RequestMetricsService {
  //Metric names
  public static final String OUTGOING_REQUESTS = "outgoing.requests";
  public static final String INCOMING_REQUESTS = "incoming.requests";

  //Headers
  public static final String FROM_SERVICE_HEADER = "X-from-service";
  public static final String TO_SERVICE_HEADER = "X-to-service";
  //Outcome
  public static final String INFORMATIONAL = "INFORMATIONAL";
  public static final String REDIRECTION = "REDIRECTION";
  public static final String SUCCESS = "SUCCESS";
  public static final String CLIENT_ERROR = "CLIENT_ERROR";
  public static final String SERVER_ERROR = "SERVER_ERROR";
  //Outcome tags
  public static final Tag OUTCOME_SUCCESS = Tag.of("outcome", "SUCCESS");
  public static final Tag OUTCOME_CLIENT_ERROR = Tag.of("outcome", "CLIENT_ERROR");
  public static final Tag OUTCOME_SERVER_ERROR = Tag.of("outcome", "SERVER_ERROR");
  //Node names
  public static final String INTERNET = "INTERNET";
  public static final Tag SERVICE_ID_INTERNET = Tag.of("serviceId", INTERNET);

  private static final long DEFAULT_WINDOW = 60_000L;
  // If the metrics grow beyond this point we flush it to prevent from OOM
  private static final long MAX_SIZE = 65_000L;

  private final AtomicBoolean isUpdateMetricsRunning = new AtomicBoolean(false);
  /**
   * The actual atomic values per metric which are registered with micrometer
   */
  private final Map<Metric, AtomicInteger> values = new HashMap<>();
  private final long window;
  /**
   * We keep track of the metrics in the last minute
   * Because the key is a timestamp in milliseconds we might have multiple metric entries per key (timestamp)
   * Hence we keep track of a list of metrics per timestamp
   */
  private Map<Long, List<Metric>> metricsPerTimestamp = new HashMap<>();
  @Autowired
  private MeterRegistry meterRegistry;

  public RequestMetricsService() {
    this(DEFAULT_WINDOW);
  }

  public RequestMetricsService(long window) {
    this.window = window;
  }

  /**
   * With a fixed rate (for example 2 seconds) the
   */
  @Scheduled(fixedRateString = "${request-metrics.update-rate-millis:2000}")
  public void updateMetrics() {
    if (isUpdateMetricsRunning.compareAndSet(false, true)) {
      try {
        updateMetricsSynch();
      } finally {
        isUpdateMetricsRunning.set(false);
      }
    }
  }

  private void updateMetricsSynch() {
    long start = System.currentTimeMillis();
    try {
      log.debug("Updating metrics");
      //Per metric in the last minute we want to determine a count
      final Map<Metric, Integer> countPerMetric = new HashMap<>();

      // We keep track of keys to be removed because they are outdated here and remove if needed.
      Set<Long> toBeRemoved = new HashSet<>();
      metricsPerTimestamp.keySet().forEach(timestamp -> {
        boolean outdated = System.currentTimeMillis() - timestamp > window;
        if (outdated) {
          toBeRemoved.add(timestamp);
        } else {
          List<Metric> metrics = metricsPerTimestamp.get(timestamp);
          if (metrics != null) {
            metrics.forEach(metric -> {
              if (metric != null) {
                Integer count = countPerMetric.get(metric);
                if (count == null) {
                  count = 0;
                }
                countPerMetric.put(metric, count + 1);
              }
            });
          }
        }
      });
      log.debug("Count per metric contains {} items", countPerMetric.size());
      //Remove the outdated timestamps
      log.debug("Removing {} items from metricsPerTimestamp", toBeRemoved.size());
      toBeRemoved.forEach(metricsPerTimestamp::remove);

      // For every metric we keep track of the count as an AtomicInteger in the values map
      // Counts are automatically added here but also put to 0 if not seen in the last time window.
      Set<Metric> notHandled = new HashSet<>(values.keySet());
      countPerMetric.forEach((metric, count) -> {
        log.debug("Metric '{}' for tags {} last minute is {}", metric.getName(), metric.getTags(), count);
        AtomicInteger value = values.get(metric);
        if (value == null) {
          value = meterRegistry.gauge(metric.getName(), metric.getTags(), new AtomicInteger());
          values.put(metric, value);
        }
        value.set(count);
        notHandled.remove(metric);
      });

      notHandled.forEach(metric -> {
        values.get(metric).set(0);
      });
    } catch (ConcurrentModificationException e) {
      //Ignore we will get a result next iteration
      log.debug("Ignoring this exception", e);
    } catch (Exception e) {
      log.error("Unable to update metrics", e);
    }

    log.debug("Currently we have {} items in the metricsPerTimestamp", metricsPerTimestamp.size());
    log.debug("Currently we have {} items in the values", values.size());

    if (metricsPerTimestamp.size() > MAX_SIZE) {
      log.warn("Cleaning up is not properly done because we have more metrics then {}. Flushing all.", MAX_SIZE);
      metricsPerTimestamp = new HashMap<>();
    }
    log.debug("Took {}ms to update metrics", System.currentTimeMillis() - start);
  }

  /**
   * Register an outgoing request to a service with a certain outcome
   *
   * @param toService
   * @param request
   * @param outcome
   */
  public void outgoingRequest(String toService, String request, String outcome) {
    updateRequestRateMetric(OUTGOING_REQUESTS, toService, request, outcome);
  }

  /**
   * Register an incoming request from a service with a certain outcome
   *
   * @param fromService
   * @param request
   * @param outcome
   */
  public void incomingRequest(String fromService, String request, String outcome) {
    if (request != null && request.toLowerCase().contains("/keepalive")) {
      return;
    }
    updateRequestRateMetric(INCOMING_REQUESTS, fromService, request, outcome);
  }

  private void updateRequestRateMetric(String metricName, String serviceId, String request, String outcome) {
    //If the serviceId is null we assume traffic is coming from the internet
    serviceId = serviceId == null ? INTERNET : serviceId;
    log.trace("Recording metric '{}' for service '{}' request '{}' and outcome '{}'", metricName, serviceId, request, outcome);
    long timestamp = System.currentTimeMillis();
    List<Metric> list = metricsPerTimestamp.computeIfAbsent(timestamp, k -> new ArrayList<>());
    list.add(new Metric(metricName, Tags.of(Tag.of("serviceId", serviceId), Tag.of("outcome", outcome))));
  }

  Map<Long, List<Metric>> getMetricsPerTimestamp() {
    return metricsPerTimestamp;
  }

  Map<Metric, AtomicInteger> getValues() {
    return values;
  }
}
