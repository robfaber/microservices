package nl.robfaber.quote.fetcher.config;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("quote-fetcher")
@Data
@RequiredArgsConstructor
public class QuoteFetcherProperties {
  private String[] ingSymbols;
  private String[] yahooSymbols;
}
