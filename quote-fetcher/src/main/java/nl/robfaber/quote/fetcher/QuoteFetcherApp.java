package nl.robfaber.quote.fetcher;

import nl.rofaber.metrics.annotations.EnableRequestMetrics;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableDiscoveryClient
@EnableScheduling
@EnableRequestMetrics
public class QuoteFetcherApp {
  public static void main(String[] args) {
    SpringApplication.run(QuoteFetcherApp.class, args);
  }
}
