package nl.robfaber.quote.fetcher.services.yahoo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import nl.robfaber.quote.fetcher.config.QuoteFetcherProperties;
import nl.robfaber.quote.model.Quote;
import nl.robfaber.quote.model.Stock;
import nl.rofaber.metrics.RequestMetricsService;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import yahoofinance.YahooFinance;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class YahooQuoteFetcher {

  private final QuoteFetcherProperties properties;
  private final KafkaTemplate<String, Object> kafkaTemplate;
  private final RequestMetricsService requestMetricsService;

  private Map<String, yahoofinance.Stock> stocks;

  @Scheduled(fixedRate = 60_000L)
  public void fetch() {
    try {
      stocks = YahooFinance.get(properties.getYahooSymbols());
      requestMetricsService.outgoingRequest("yahoo-finance", "get-stocks", RequestMetricsService.SUCCESS);
    } catch (IOException e) {
      log.info("Failed to retrieve stocks", e);
      requestMetricsService.outgoingRequest("yahoo-finance", "get-stocks", RequestMetricsService.SERVER_ERROR);
    }

    stocks.values().forEach(stock -> {
      val mappedStock = map(stock);
      val key = mappedStock.getSymbol();
      try {
        kafkaTemplate.send("stocks", key, mappedStock);
        requestMetricsService.outgoingRequest("kafka", "stocks", RequestMetricsService.SUCCESS);
      } catch (Exception e) {
        log.error("Failed to publish to kafka", e);
        requestMetricsService.outgoingRequest("kafka", "stocks", RequestMetricsService.SERVER_ERROR);
      }
      try {
        kafkaTemplate.send("quotes", key, mappedStock.getQuote());
        requestMetricsService.outgoingRequest("kafka", "quotes", RequestMetricsService.SUCCESS);
      } catch (Exception e) {
        log.error("Failed to publish to kafka", e);
        requestMetricsService.outgoingRequest("kafka", "quotes", RequestMetricsService.SERVER_ERROR);
      }
    });
  }

  public Stock getStock(String symbol) {
    if (stocks == null) {
      return null;
    }
    return map(stocks.get(symbol));
  }

  public List<Stock> getStocks() {
    if (stocks == null) {
      return Collections.emptyList();
    }
    return stocks.values().stream()
        .map(this::map)
        .collect(Collectors.toList());
  }

  private Stock map(yahoofinance.Stock stock) {
    val quote = stock.getQuote();
    val lastTrade = ZonedDateTime.ofInstant(
        quote.getLastTradeTime().toInstant(),
        ZoneId.of(quote.getTimeZone().getID()));
    return Stock.builder()
        .symbol(stock.getSymbol())
        .name(stock.getName())
        .stockExchange(stock.getStockExchange())
        .currency(stock.getCurrency())
        .quote(Quote.builder()
            .symbol(quote.getSymbol())
            .price(quote.getPrice())
            .open(quote.getOpen())
            .dayHigh(quote.getDayHigh())
            .dayLow(quote.getDayLow())
            .lastTrade(lastTrade)
            .previousClose(quote.getPreviousClose())
            .build())
        .build();
  }
}
