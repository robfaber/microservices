package contracts.hello

org.springframework.cloud.contract.spec.Contract.make {
    request {
        method 'GET'
        url '/hello'
        headers {
            header('Content-Type', 'application/json')
        }
    }
    response {
        status 200
        body("""
          { "value": "Hello"}
          """)
        headers {
            header(
                    'Content-Type': 'application/json',
            )
        }
    }
}