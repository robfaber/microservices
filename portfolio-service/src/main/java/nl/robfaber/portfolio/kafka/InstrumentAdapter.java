package nl.robfaber.portfolio.kafka;

import lombok.val;
import nl.robfaber.portfolio.domain.Instrument;
import nl.robfaber.portfolio.domain.Money;
import nl.robfaber.portfolio.ports.InstrumentPort;
import nl.robfaber.quote.model.Stock;
import nl.rofaber.metrics.RequestMetricsService;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static nl.robfaber.portfolio.kafka.KafkaConfig.STOCKS_STORE;

@Service
public class InstrumentAdapter implements InstrumentPort {
  private Optional<RequestMetricsService> requestMetricsService;
  private final StreamsBuilderFactoryBean factoryBean;
  private ReadOnlyKeyValueStore<String, Stock> stockStore;

  public InstrumentAdapter(Optional<RequestMetricsService> requestMetricsService, StreamsBuilderFactoryBean factoryBean) {
    this.requestMetricsService = requestMetricsService;
    this.factoryBean = factoryBean;
  }

  @Override
  public List<Instrument> getInstruments() {
    try {
      List<Instrument> instruments = new ArrayList<>();
      stockStore().all().forEachRemaining(keyValue -> instruments.add(map(keyValue.value)));
      requestMetricsService.ifPresent((service) -> service.outgoingRequest("kafka", "get-stocks", RequestMetricsService.SUCCESS));
      return instruments;
    } catch (RuntimeException e) {
      requestMetricsService.ifPresent((service -> service.outgoingRequest("kafka", "get-stocks", RequestMetricsService.SERVER_ERROR)));
      throw e;
    }
  }

  @Override
  public Instrument getInstrument(String symbol) {
    val stock = getStock(symbol);
    return map(stock);
  }

  private Instrument map(Stock stock) {
    return Instrument.builder()
        .name(stock.getName())
        .symbol(stock.getSymbol())
        .stockExchange(stock.getStockExchange())
        .currentPrice(Money.builder()
            .currency(stock.getCurrency())
            .amount(stock.getQuote().getPrice())
            .dateTime(stock.getQuote().getLastTrade())
            .build())
        .build();
  }

  private Stock getStock(String symbol) {
    try {
      val stock = stockStore().get(symbol);
      if (stock == null) {
        requestMetricsService.ifPresent((service) -> service.outgoingRequest("kafka", "get-stock", RequestMetricsService.SERVER_ERROR));
        throw new IllegalArgumentException("Stock not found.");
      } else {
        requestMetricsService.ifPresent((service) -> service.outgoingRequest("kafka", "get-stock", RequestMetricsService.SUCCESS));
      }
      return stock;
    } catch (RuntimeException e) {
      requestMetricsService.ifPresent((service) -> service.outgoingRequest("kafka", "get-stock", RequestMetricsService.SERVER_ERROR));
      throw e;
    }
  }

  private ReadOnlyKeyValueStore<String, Stock> stockStore() {
    if (stockStore == null) {
      val kafkaStreams = factoryBean.getKafkaStreams();
      stockStore = kafkaStreams.store(StoreQueryParameters.fromNameAndType(STOCKS_STORE, QueryableStoreTypes.keyValueStore()));
    }
    return stockStore;
  }
}
