package nl.robfaber.portfolio.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.robfaber.quote.model.Stock;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Named;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerde;

import java.util.Map;

@Configuration
@EnableKafka
@EnableKafkaStreams
@Slf4j
@RequiredArgsConstructor
public class KafkaConfig {
  public static final String STOCKS_TOPIC = "stocks";
  public static final String STOCKS_STORE = "stocks-store";
  public static final String CURRENT_STOCKS_TOPIC = "current-stocks";

  private final KafkaProperties kafkaProperties;

  @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
  public KafkaStreamsConfiguration kStreamMapConfig() {
    Map<String, Object> props = kafkaProperties.buildStreamsProperties();
    props.put(StreamsConfig.APPLICATION_ID_CONFIG, "portfolio-service");
    props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
    props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, JsonSerde.class);
    props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
    props.put(JsonDeserializer.VALUE_DEFAULT_TYPE, Stock.class);

    return new KafkaStreamsConfiguration(props);
  }

  @Bean
  public KStream<String, Stock> currentStocksStream(StreamsBuilder streamsBuilder) {
    KStream<String, Stock> stocks = streamsBuilder.stream(STOCKS_TOPIC);

    KTable<String, Stock> currentStocksTable = stocks
        .map((key, stock) -> new KeyValue<String, Stock>(key, stock))
        .groupByKey()
        .reduce((stock1, stock2) -> mostRecentStock(stock1, stock2), Materialized.as(STOCKS_STORE));

    return currentStocksTable.toStream(Named.as(CURRENT_STOCKS_TOPIC));
  }

  private Stock mostRecentStock(Stock stock1, Stock stock2) {
    if (stock1.getQuote() == null) {
      return stock2;
    }
    if (stock2.getQuote() == null) {
      return stock1;
    }
    return stock1.getQuote().getLastTrade().isBefore(stock2.getQuote().getLastTrade()) ? stock2 : stock1;
  }
}
