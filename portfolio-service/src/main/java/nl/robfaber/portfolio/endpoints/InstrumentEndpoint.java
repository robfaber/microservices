package nl.robfaber.portfolio.endpoints;

import lombok.RequiredArgsConstructor;
import nl.robfaber.portfolio.database.entities.InstrumentEntity;
import nl.robfaber.portfolio.database.repository.InstrumentRepository;
import nl.robfaber.portfolio.domain.Instrument;
import nl.robfaber.portfolio.ports.InstrumentPort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/instruments")
@RequiredArgsConstructor
public class InstrumentEndpoint {
  private final InstrumentRepository repository;
  private final InstrumentPort instrumentPort;

  @GetMapping("/database")
  public ResponseEntity<List<InstrumentEntity>> getInstrumentsFromDatabase() {
    return ResponseEntity.ok(repository.findAll());
  }

  @GetMapping
  public ResponseEntity<List<Instrument>> getInstruments() {
    return ResponseEntity.ok(instrumentPort.getInstruments());
  }

  @GetMapping("/{symbol}")
  public ResponseEntity<Instrument> getInstrument(@PathVariable("symbol") String symbol) {
    return ResponseEntity.ok(instrumentPort.getInstrument(symbol));
  }
}
