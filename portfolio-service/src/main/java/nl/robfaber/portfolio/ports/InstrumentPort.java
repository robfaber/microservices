package nl.robfaber.portfolio.ports;

import nl.robfaber.portfolio.domain.Instrument;

import java.util.List;

public interface InstrumentPort {
  List<Instrument> getInstruments();

  Instrument getInstrument(String symbol);
}
