package nl.robfaber.portfolio.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Instrument {
  private String name;
  private String stockExchange;
  private String symbol;
  private Money currentPrice;
}
