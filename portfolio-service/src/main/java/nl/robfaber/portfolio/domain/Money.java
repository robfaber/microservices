package nl.robfaber.portfolio.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Money {
  private String currency;
  private BigDecimal amount;
  private ZonedDateTime dateTime;


  public Money times(BigDecimal multiplier) {
    return Money.builder()
        .currency(this.getCurrency())
        .amount(this.getAmount().multiply(multiplier))
        .dateTime(this.getDateTime())
        .build();
  }
}
