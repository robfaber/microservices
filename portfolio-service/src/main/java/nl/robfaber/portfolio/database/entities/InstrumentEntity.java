package nl.robfaber.portfolio.database.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "INSTRUMENTS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InstrumentEntity {
  @Id
  @GeneratedValue
  private long id;

  private String name;
  private String symbol;
}
