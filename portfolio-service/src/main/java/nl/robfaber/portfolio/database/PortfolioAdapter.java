package nl.robfaber.portfolio.database;

import lombok.RequiredArgsConstructor;
import lombok.val;
import nl.robfaber.portfolio.database.entities.PortfolioEntity;
import nl.robfaber.portfolio.database.entities.PositionEntity;
import nl.robfaber.portfolio.database.repository.PortfolioRepository;
import nl.robfaber.portfolio.domain.Portfolio;
import nl.robfaber.portfolio.domain.Position;
import nl.robfaber.portfolio.ports.InstrumentPort;
import nl.robfaber.portfolio.ports.PortfolioPort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PortfolioAdapter implements PortfolioPort {
  private final PortfolioRepository portfolioRepository;
  private final InstrumentPort instrumentPort;

  @Override
  public List<Portfolio> getPortfolios() {
    return portfolioRepository.findAll()
        .stream()
        .map(this::map)
        .collect(Collectors.toList());
  }

  @Override
  public Portfolio getPortfolio(Long id) {
    return map(portfolioRepository.getById(id));
  }

  @Override
  public void savePortfolio(Portfolio portfolio) {
    PortfolioEntity portfolioEntity;
    if (portfolio.getId() != null) {
      portfolioEntity = portfolioRepository.getById(portfolio.getId());
    } else {
      portfolioEntity = new PortfolioEntity();
    }
    portfolioEntity.setName(portfolio.getName());
//    portfolioEntity.setPositions();
    portfolioRepository.save(portfolioEntity);
  }

  @Override
  public void deletePortfolio(Portfolio portfolio) {
    val portfolioEntity = portfolioRepository.getById(portfolio.getId());
    portfolioRepository.delete(portfolioEntity);
  }

  private Portfolio map(PortfolioEntity portfolioEntity) {
    return Portfolio.builder()
        .id(portfolioEntity.getId())
        .name(portfolioEntity.getName())
        .positions(map(portfolioEntity.getPositions()))
        .build();
  }

  private List<Position> map(Set<PositionEntity> positionEntities) {
    return positionEntities.stream()
        .map(this::map)
        .collect(Collectors.toList());
  }

  private Position map(PositionEntity positionEntity) {
    val instrument = instrumentPort.getInstrument(positionEntity.getSymbol());
    return Position.builder()
        .id(positionEntity.getId())
        .instrument(instrument)
        .quantity(positionEntity.getQuantity())
        .currentValue(instrument.getCurrentPrice() != null ? instrument.getCurrentPrice().times(positionEntity.getQuantity()) : null)
        .build();
  }
}
