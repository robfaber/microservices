package nl.robfaber.portfolio.database.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "POSITIONS")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PositionEntity {
  @Id
  @GeneratedValue
  private long id;

  private BigDecimal quantity;
  private String symbol;
}
