package nl.robfaber.portfolio.database.repository;

import nl.robfaber.portfolio.database.entities.InstrumentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstrumentRepository extends JpaRepository<InstrumentEntity, Long> {
}
