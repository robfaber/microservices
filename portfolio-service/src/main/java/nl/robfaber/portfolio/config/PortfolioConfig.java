package nl.robfaber.portfolio.config;

import nl.rofaber.metrics.annotations.EnableJpaMetrics;
import nl.rofaber.metrics.annotations.EnableRequestMetrics;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@EnableRequestMetrics
@EnableJpaMetrics
public class PortfolioConfig {

//  @Value("${spring.application.name}")
//  private String applicationName;
//
//  @Bean
//  public MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() throws UnknownHostException {
//    List<Tag> tags = Arrays.asList(
//        Tag.of("application", applicationName),
//        Tag.of("host", InetAddress.getLocalHost().getHostName())
//    );
//
//    return registry -> registry.config().commonTags(tags);
//  }
}
