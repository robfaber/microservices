mvn clean install -DskipTests

pack build robfaber/portfolio-service --path target/portfolio-service-1.1.0-SNAPSHOT.jar --builder paketobuildpacks/builder:base

cd .. && docker-compose up -d
cd portfolio-service
