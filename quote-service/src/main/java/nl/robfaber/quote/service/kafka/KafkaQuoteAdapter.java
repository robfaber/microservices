package nl.robfaber.quote.service.kafka;

import lombok.RequiredArgsConstructor;
import lombok.val;
import nl.robfaber.quote.model.Quote;
import nl.robfaber.quote.service.port.QuotePort;
import nl.rofaber.metrics.RequestMetricsService;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static nl.robfaber.quote.service.config.QuoteServiceConfig.LAST_QUOTES_STORE;

@Service
@RequiredArgsConstructor
public class KafkaQuoteAdapter implements QuotePort {
  private final RequestMetricsService requestMetricsService;
  private final StreamsBuilderFactoryBean factoryBean;
  private ReadOnlyKeyValueStore<String, Quote> quoteStore;

  @Override
  public List<Quote> getQuotes() {
    try {
      List<Quote> quotes = new ArrayList<>();
      store().all().forEachRemaining(keyValue -> quotes.add(keyValue.value));
      requestMetricsService.outgoingRequest("kafka", "get-quotes", RequestMetricsService.SUCCESS);
      return quotes;
    } catch (RuntimeException e) {
      requestMetricsService.outgoingRequest("kafka", "get-quotes", RequestMetricsService.SERVER_ERROR);
      throw e;
    }
  }

  @Override
  public Quote getQuote(String symbol) {
    try {
      val quote = store().get(symbol);
      if (quote == null) {
        requestMetricsService.outgoingRequest("kafka", "get-quote", RequestMetricsService.CLIENT_ERROR);
        throw new IllegalArgumentException("Quote not found.");
      } else {
        requestMetricsService.outgoingRequest("kafka", "get-quote", RequestMetricsService.SUCCESS);
      }
      return quote;
    } catch (RuntimeException e) {
      requestMetricsService.outgoingRequest("kafka", "get-quote", RequestMetricsService.SERVER_ERROR);
      throw e;
    }
  }

  private ReadOnlyKeyValueStore<String, Quote> store() {
    if (quoteStore == null) {
      val kafkaStreams = factoryBean.getKafkaStreams();
      quoteStore = kafkaStreams.store(StoreQueryParameters.fromNameAndType(LAST_QUOTES_STORE, QueryableStoreTypes.keyValueStore()));
    }
    return quoteStore;
  }
}


