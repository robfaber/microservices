package nl.robfaber.quote.service.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.robfaber.quote.model.Quote;
import nl.rofaber.metrics.annotations.EnableRequestMetrics;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Named;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.config.KafkaStreamsConfiguration;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerde;

import java.util.Map;

@Configuration
@EnableKafka
@EnableKafkaStreams
@Slf4j
@EnableRequestMetrics
@RequiredArgsConstructor
public class QuoteServiceConfig {
  public static final String STOCKS_TOPIC = "stocks";
  public static final String QUOTES_TOPIC = "quotes";
  public static final String LAST_QUOTES_STORE = "last-quotes-store";
  public static final String LAST_QUOTES_TOPIC = "last-quotes";

  private final KafkaProperties kafkaProperties;

  @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
  public KafkaStreamsConfiguration kStreamMapConfig() {
    Map<String, Object> props = kafkaProperties.buildStreamsProperties();
    props.put(StreamsConfig.APPLICATION_ID_CONFIG, "quotes-service");
    props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
    props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, JsonSerde.class);
    props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
    props.put(JsonDeserializer.VALUE_DEFAULT_TYPE, Quote.class);
    return new KafkaStreamsConfiguration(props);
  }

  @Bean
  public KStream<String, Quote> lastQuoteStream(StreamsBuilder streamsBuilder) {
    KStream<String, Quote> quotes = streamsBuilder.stream(QUOTES_TOPIC);

    KTable<String, Quote> lastQuotesTable = quotes
        .map((key, quote) -> new KeyValue<String, Quote>(key, quote))
        .groupByKey()
        .reduce((quote1, quote2) -> lastQuote(quote1, quote2), Materialized.as(LAST_QUOTES_STORE));

    return lastQuotesTable.toStream(Named.as(LAST_QUOTES_TOPIC));
  }

  private Quote lastQuote(Quote quote1, Quote quote2) {
    if (quote1 == null) {
      return quote2;
    }
    if (quote2 == null) {
      return quote1;
    }
    return quote1.getLastTrade().isBefore(quote2.getLastTrade()) ? quote2 : quote1;
  }
}
