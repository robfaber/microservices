package nl.robfaber.quote.service.listeners;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.robfaber.quote.model.Quote;
import nl.robfaber.quote.model.Stock;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import static nl.robfaber.quote.service.config.QuoteServiceConfig.LAST_QUOTES_TOPIC;
import static nl.robfaber.quote.service.config.QuoteServiceConfig.QUOTES_TOPIC;
import static nl.robfaber.quote.service.config.QuoteServiceConfig.STOCKS_TOPIC;

@Service
@RequiredArgsConstructor
@Slf4j
public class QuoteListener {

  @KafkaListener(id = "stock-listener", topics = STOCKS_TOPIC)
  public void handleStock(ConsumerRecord<String, Stock> stock) {
    log.info("Stock update: {} -> {}", stock.key(), stock.value());
  }

  @KafkaListener(id = "quote-listener", topics = QUOTES_TOPIC)
  public void handleQuote(ConsumerRecord<String, Quote> quote) {
    log.info("Quote update: {} -> {}", quote.key(), quote.value());
  }

  @KafkaListener(id = "last-quote-listener", topics = LAST_QUOTES_TOPIC)
  public void handleLastQuote(ConsumerRecord<String, Quote> quote) {
    log.info("Last Quote update: {} -> {}", quote.key(), quote.value());
  }
}
