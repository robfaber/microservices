package nl.robfaber.quote.service.port;

import nl.robfaber.quote.model.Quote;

import java.util.List;

public interface QuotePort {
  List<Quote> getQuotes();

  Quote getQuote(String symbol);
}
