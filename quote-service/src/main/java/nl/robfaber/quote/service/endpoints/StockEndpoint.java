package nl.robfaber.quote.service.endpoints;

import lombok.RequiredArgsConstructor;
import nl.robfaber.quote.model.Stock;
import nl.robfaber.quote.service.port.QuotePort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/stocks")
public class StockEndpoint {
  private final QuotePort quotePort;

  @GetMapping
  public List<Stock> stocks() {
    return Collections.emptyList();
//    return quotePort.getStocks();
  }
}
