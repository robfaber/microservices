package nl.robfaber.quote.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Stock {
  private String symbol;
  private String name;
  private String currency;
  private String stockExchange;
  private Quote quote;
}
