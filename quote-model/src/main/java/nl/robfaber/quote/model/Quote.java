package nl.robfaber.quote.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Quote {
  private String symbol;
  private ZonedDateTime lastTrade;
  //  private String timeZone; //America/New_York",
  //  private Double ask; //1185.15,
//  private Integer askSize; //9,
//  private Double bid; //1183.36,
//  private Integer bidSize; //12,
  private BigDecimal price; //1182.5599,
  //  private Integer lastTradeSize; //null,
//  private String lastTradeDateStr; //null,
//  private String lastTradeTimeStr; //null,
//  private String lastTradeTime; //2021-11-22T20:34:49.000+00:00",
  private BigDecimal open; //1162.33,
  private BigDecimal previousClose; //1137.06,
  private BigDecimal dayLow; //1132.8201,
  private BigDecimal dayHigh; //1201.94,
//  private Double yearLow; //501.79,
//  private Double yearHigh; //1243.49,
//  private Double priceAvg50; //917.374,
//  private Double priceAvg200; //736.90594,
//  private Long volume; //29067178,
//  private Long avgVolume; //23898237,
//  private Double changeFromYearHigh; //-60.9301,
//  private Double changeFromYearHighInPercent; //-4.9,
//  private Double changeFromAvg50; //265.1859,
//  private Double changeFromAvg50InPercent; //28.91,
//  private Double changeFromAvg200; //445.65396,
//  private Double changeFromAvg200InPercent; //60.48,
//  private Double change; //45.4999,
//  private Double changeInPercent; //4,
//  private Double changeFromYearLow; //680.7699,
//  private Double changeFromYearLowInPercent; //135.67
}
