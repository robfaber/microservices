crt_branch=`git rev-parse --abbrev-ref HEAD`

# Bump version to the next snapshot
version=$(grep '<project' pom.xml -A5 | grep '<version>' | egrep -o '[0-9]+.[0-9]+.[0-9]+')
major=$(echo $version | sed 's?\([0-9][0-9]*\).[0-9][0-9]*.[0-9][0-9]*?\1?')
minor=$(echo $version | sed 's?[0-9][0-9]*.\([0-9][0-9]*\).[0-9][0-9]*?\1?')
patch=$(echo $version | sed 's?[0-9][0-9]*.[0-9][0-9]*.\([0-9][0-9]*\)?\1?')
echo "Found current version $version"

  case "$1" in
    major) major=$(( 10#$major + 1 )); minor=00; patch=00 ;;
    minor) minor=$(( 10#$minor + 1 )); patch=00 ;;
    patch) patch=$(( 10#$patch + 1 )) ;;
    *) echo "call with <major|minor|patch>"; exit 1 ;;
  esac

#patch=$(( 10#$patch + 1 )) ;
next_version=$(printf "%d.%d.%d" $major $minor $patch)
echo "Setting to $next_version"

branch="snapshot-$next_version"
git fetch && git checkout origin/master && git checkout -b $branch

mvn versions:set -DnewVersion=${next_version}-SNAPSHOT versions:commit
git add '**/pom.xml'
git add './pom.xml'
git commit -m "Set version to $next_version-SNAPSHOT"

mr_url=`git push --set-upstream origin $branch 2>&1 | grep merge_requests | grep -o 'http.*'`

echo "opening $mr_url"
open -a "/Applications/Google Chrome.app" $mr_url
if [[ $? != 0 ]]; then
  echo "Could not open MR URL. Please go to: $mr_url"
fi

git checkout $crt_branch
git branch -d $branch