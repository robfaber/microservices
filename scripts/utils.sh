#!/bin/bash -e

export MAVEN_OPTS="-Djava.awt.headless=true -Xms256m -Xmx2048m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -Xss512k"

# Set maven version to include hash if running in gitlab ci
function set_unique_maven_version {
    if [ ! -z ${GITLAB_CI+x} ]; then
        hash=$(git rev-parse HEAD | cut -c 1-6)
        version=$(grep '<project' pom.xml -A5 | grep '<version>' | egrep -o '[0-9]+.[0-9]+.[0-9]+')
        newVersion="$version-$hash-SNAPSHOT"
        mvn versions:set -DnewVersion=${newVersion} --batch-mode
    fi
}
function set_unique_maven_version_on_branch {
    branch="$CI_BUILD_REF_NAME"
    echo "On branch '$branch'"

    if [ "master" != $branch ];
    then
        echo "Making build unique"
        set_unique_maven_version
    fi
}
