## How To release with maven and git


1. Run the `release.sh` script this will;
    - Sets the version to a release (without the SNAPSHOT)
    - Add and commits the changes to git
    - Creates a git tag for this version
    - Performs a deploy build to distribute the artifacts to artifactory
2. Run the `bump-version.sh <major|minor|patch>` script to;
    - Update the version to the next SNAPSHOT 
    - Pushes changes to git
