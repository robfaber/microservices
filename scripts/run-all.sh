EUREKA_DCR="http://localhost:8762/eureka"
EUREKA_WPR="http://localhost:8763/eureka"

MEM="-Xmx64m -Xms64m"
OPTS_DCR="$MEM -Deureka.client.serviceUrl.defaultZone=$EUREKA_DCR -Deureka.instance.metadataMap.datacenter=DCR"
OPTS_WPR="$MEM -Deureka.client.serviceUrl.defaultZone=$EUREKA_WPR -Deureka.instance.metadataMap.datacenter=WPR"

(cd service-registry && java $OPTS_DCR -Deureka.datacenter=DCR -Dserver.port=8762 -jar target/service-registry-1.1.0-SNAPSHOT.jar >| ../service-registry-dcr.log 2>&1 ) &
(cd service-registry && java $OPTS_WPR -Deureka.datacenter=WPR -Dserver.port=8763 -jar target/service-registry-1.1.0-SNAPSHOT.jar >| ../service-registry-wpr.log 2>&1 ) &
./scripts/wait-for-it.sh -t 30 -h localhost -p 8762
./scripts/wait-for-it.sh -t 30 -h localhost -p 8763

(cd config-server && java $OPTS_DCR -Dserver.port=8880 -jar target/config-server-1.1.0-SNAPSHOT.jar >| ../config-server-dcr.log 2>&1 ) &
(cd config-server && java $OPTS_WPR -Dserver.port=8881 -jar target/config-server-1.1.0-SNAPSHOT.jar >| ../config-server-wpr.log 2>&1 ) &
./scripts/wait-for-it.sh -t 30 -h localhost -p 8880
./scripts/wait-for-it.sh -t 30 -h localhost -p 8881

(cd api-gateway && java $OPTS_DCR -Dserver.port=8091 -Dspring.profiles.active=local -jar target/api-gateway-1.1.0-SNAPSHOT.jar >| ../api-gateway-dcr.log 2>&1 ) &
(cd api-gateway && java $OPTS_WPR -Dserver.port=8092 -Dspring.profiles.active=local -jar target/api-gateway-1.1.0-SNAPSHOT.jar >| ../api-gateway-wpr.log 2>&1 ) &

(cd admin-server && java $OPTS_DCR -Dserver.port=8870 -jar target/admin-server-1.1.0-SNAPSHOT.jar >| ../admin-server-dcr.log 2>&1 ) &
(cd admin-server && java $OPTS_WPR -Dserver.port=8871 -jar target/admin-server-1.1.0-SNAPSHOT.jar >| ../admin-server-wpr.log 2>&1 ) &

(cd portfolio-service && java $OPTS_DCR -Dserver.port=9001 -jar target/portfolio-service-1.1.0-SNAPSHOT.jar >| ../capability-1-dcr.log 2>&1 ) &
(cd portfolio-service && java $OPTS_DCR -Dserver.port=9002 -jar target/portfolio-service-1.1.0-SNAPSHOT.jar >| ../capability-2-dcr.log 2>&1 ) &
(cd portfolio-service && java $OPTS_WPR -Dserver.port=9003 -jar target/portfolio-service-1.1.0-SNAPSHOT.jar >| ../capability-1-wpr.log 2>&1 ) &
(cd portfolio-service && java $OPTS_WPR -Dserver.port=9004 -jar target/portfolio-service-1.1.0-SNAPSHOT.jar >| ../capability-2-wpr.log 2>&1 ) &

(cd experience-service && java $OPTS_DCR -Dserver.port=9010 -jar target/experience-service-1.1.0-SNAPSHOT.jar >| ../experience-1-dcr.log 2>&1 ) &
(cd experience-service && java $OPTS_DCR -Dserver.port=9011 -jar target/experience-service-1.1.0-SNAPSHOT.jar >| ../experience-2-dcr.log 2>&1 ) &
(cd experience-service && java $OPTS_WPR -Dserver.port=9012 -jar target/experience-service-1.1.0-SNAPSHOT.jar >| ../experience-1-wpr.log 2>&1 ) &
(cd experience-service && java $OPTS_WPR -Dserver.port=9013 -jar target/experience-service-1.1.0-SNAPSHOT.jar >| ../experience-2-wpr.log 2>&1 ) &

(cd vizceral-server && java $OPTS_DCR -Dserver.port=8886 -Dspring.profiles.active=multi -jar target/vizceral-server-1.1.0-SNAPSHOT.jar >| ../vizceral-server.log 2>&1 ) &

./scripts/wait-for-it.sh -t 30 -h localhost -p 8091
./scripts/wait-for-it.sh -t 30 -h localhost -p 8092
./scripts/wait-for-it.sh -t 30 -h localhost -p 8870
./scripts/wait-for-it.sh -t 30 -h localhost -p 8871
./scripts/wait-for-it.sh -t 30 -h localhost -p 9001
./scripts/wait-for-it.sh -t 30 -h localhost -p 9002
./scripts/wait-for-it.sh -t 30 -h localhost -p 9003
./scripts/wait-for-it.sh -t 30 -h localhost -p 9004
./scripts/wait-for-it.sh -t 30 -h localhost -p 9010
./scripts/wait-for-it.sh -t 30 -h localhost -p 9011
./scripts/wait-for-it.sh -t 30 -h localhost -p 9012
./scripts/wait-for-it.sh -t 30 -h localhost -p 9013
./scripts/wait-for-it.sh -t 30 -h localhost -p 8886
