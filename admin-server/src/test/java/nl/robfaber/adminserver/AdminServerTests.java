package nl.robfaber.adminserver;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {
  "eureka.client.enabled=false",
  "spring.cloud.config.enabled=false",
  "spring.cloud.config.discovery.enabled=false",
})
public class AdminServerTests {

	@Test
	public void contextLoads() {
	}

}
