package nl.robfaber.reactive.config;

import lombok.RequiredArgsConstructor;
import lombok.val;
import nl.rofaber.metrics.OutcomeUtil;
import nl.rofaber.metrics.RequestMetricsService;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;

import java.util.concurrent.atomic.AtomicBoolean;

import static nl.rofaber.metrics.RequestMetricsService.SERVER_ERROR;

@Component
@RequiredArgsConstructor
public class ReactiveOutgoingMetricsExchangeFilter implements ExchangeFilterFunction {
  private final RequestMetricsService requestMetricsService;

  @Override
  public Mono<ClientResponse> filter(ClientRequest request, ExchangeFunction next) {
    return next.exchange(request)
        .as((responseMono) -> this.handleResponse(request, responseMono));
  }

  private Mono<ClientResponse> handleResponse(ClientRequest request, Mono<ClientResponse> responseMono) {
    AtomicBoolean responseReceived = new AtomicBoolean();
    return Mono.deferContextual((ctx) -> {
      return responseMono.doOnEach((signal) -> {
        if (signal.isOnNext() || signal.isOnError()) {
          responseReceived.set(true);
          val outcome = OutcomeUtil.outcome(signal.get().rawStatusCode());
          requestMetricsService.outgoingRequest(request.url().getHost(), request.url().getPath(), outcome);
        }

      }).doFinally((signalType) -> {
        if (!responseReceived.get() && SignalType.CANCEL.equals(signalType)) {
          requestMetricsService.outgoingRequest("", request.toString(), SERVER_ERROR);
        }

      });
    });
  }
}
