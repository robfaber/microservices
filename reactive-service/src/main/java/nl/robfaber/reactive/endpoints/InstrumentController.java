package nl.robfaber.reactive.endpoints;

import lombok.RequiredArgsConstructor;
import nl.robfaber.reactive.dto.Instrument;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

@RestController
@RequiredArgsConstructor
public class InstrumentController {
  private final WebClient webClient;

  @GetMapping("/instruments")
  public Flux<Instrument> instruments() {
    return webClient
        .get().uri("http://portfolio-service/instruments")
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .bodyToFlux(Instrument.class);
  }
}
