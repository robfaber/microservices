package nl.robfaber.reactive.dto;

import lombok.Data;

@Data
public class Instrument {
  private long id;
  private String name;
}
