
docker image push registry.robfaber.nl/robfaber/service-registry
docker image push registry.robfaber.nl/robfaber/config-server
docker image push registry.robfaber.nl/robfaber/admin-server
docker image push registry.robfaber.nl/robfaber/cloud-gateway
docker image push registry.robfaber.nl/robfaber/portfolio-service
docker image push registry.robfaber.nl/robfaber/experience-service
docker image push registry.robfaber.nl/robfaber/reactive-service
docker image push registry.robfaber.nl/robfaber/vizceral-server
docker image push registry.robfaber.nl/robfaber/quote-fetcher
docker image push registry.robfaber.nl/robfaber/quote-service
