package microservices


import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class BasicSimulation extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:8090") //The root URL of the api-gateway
    .acceptHeader("application/json;") // Here are the common headers
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  val scenario1 = scenario("Test via cloud-gateway")
    .exec(http("hello").get("/example/hello"))
    .exec(http("reactive-hello").get("/reactive/hello"))
    .pause(2)
    .exec(http("instruments").get("/example/instruments"))
    .exec(http("reactive-instruments").get("/reactive/instruments"))
    .pause(2)
    .exec(http("quotes").get("/example/quotes"))
    .exec(http("reactive-quotes").get("/reactive/quotes"))

  setUp(
    scenario1.inject(
      atOnceUsers(10),
      nothingFor(5 seconds),
      rampUsers(20) during (20 seconds),
      nothingFor(5 seconds),
      constantUsersPerSec(20) during (30 seconds)
    )
  ).protocols(httpProtocol)
}
