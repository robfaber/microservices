package nl.robfaber.registry.config;

import com.netflix.discovery.DiscoveryClient;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.ClientFilter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

@Configuration
public class ServiceRegistryConfig {

  @Value("${spring.application.name}")
  private String applicationName;

  @Bean
  public MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() throws UnknownHostException {
    List<Tag> tags = Arrays.asList(
        Tag.of("application", applicationName),
        Tag.of("host", InetAddress.getLocalHost().getHostName())
    );

    return registry -> registry.config().commonTags(tags);
  }

//  @Bean
//  public RequestMetricsService requestMetricsService() {
//    return new RequestMetricsService();
//  }

//  @Bean
//  public IncomingRequestsMetricsServletFilter incomingRequestsMetricsFilter() {
//    return new IncomingRequestsMetricsServletFilter();
//  }

//  @Bean
//  public DiscoveryClient.DiscoveryClientOptionalArgs additionalArgs() {
//    DiscoveryClient.DiscoveryClientOptionalArgs args = new DiscoveryClient.DiscoveryClientOptionalArgs();
//    args.setAdditionalFilters(Arrays.asList(new ClientFilter() {
//      @Override
//      public ClientResponse handle(ClientRequest clientRequest) throws ClientHandlerException {
//        return clientRequest.;
//      }
//    }));
//    return args;
//  }

  @Bean
  public ClientFilter additionalFilter(DiscoveryClient.DiscoveryClientOptionalArgs args) {
    ClientFilter clientFilter = new ClientFilter() {
      @Override
      public ClientResponse handle(ClientRequest clientRequest) throws ClientHandlerException {
        clientRequest.getHeaders().add("X-from-service", applicationName);
        ClientResponse response = getNext().handle(clientRequest);
        return response;
      }
    };
//    DiscoveryClient.DiscoveryClientOptionalArgs args = new DiscoveryClient.DiscoveryClientOptionalArgs();
    args.setAdditionalFilters(Arrays.asList(clientFilter));
    return clientFilter;
  }
}
