load("@rules_java//java:defs.bzl", "java_binary", "java_library", "java_runtime", "java_test")
load("@rules_spring//springboot:springboot.bzl", "springboot")

package(default_visibility = ["//visibility:public"])

java_plugin(
    name = "lombok_edge_basic_plugin",
    generates_api = True,
    processor_class = "lombok.launch.AnnotationProcessorHider$AnnotationProcessor",
    visibility = ["//visibility:public"],
    deps = ["@lombok_edge//jar"],
)

java_library(
    name = "lombok_basic_edge",
    exported_plugins = [":lombok_edge_basic_plugin"],
    visibility = ["//visibility:public"],
    exports = ["@lombok_edge//jar"],
)

java_library(
    name = "service-registry-lib",
    srcs = glob([
        "src/main/java/**/*.java",
    ]),
    resources = glob([
        "src/main/resources/**",
    ]),
    deps = [
        ":lombok_basic_edge",
        "//request-metrics",
        "@maven//:ch_qos_logback_logback_classic",
        "@maven//:ch_qos_logback_logback_core",
        "@maven//:com_fasterxml_jackson_core_jackson_annotations",
        "@maven//:com_fasterxml_jackson_core_jackson_core",
        "@maven//:com_fasterxml_jackson_core_jackson_databind",
        "@maven//:com_fasterxml_jackson_dataformat_jackson_dataformat_xml",
        "@maven//:com_fasterxml_jackson_datatype_jackson_datatype_jdk8",
        "@maven//:com_fasterxml_jackson_datatype_jackson_datatype_jsr310",
        "@maven//:com_fasterxml_jackson_module_jackson_module_parameter_names",
        "@maven//:com_fasterxml_woodstox_woodstox_core",
        "@maven//:com_google_guava_guava",
        "@maven//:com_google_inject_guice",
        "@maven//:com_jayway_jsonpath_json_path",
        "@maven//:com_netflix_eureka_eureka_client",
        "@maven//:com_netflix_eureka_eureka_core",
        "@maven//:com_netflix_netflix_commons_netflix_eventbus",
        "@maven//:com_netflix_servo_servo_core",
        "@maven//:com_stoyanr_evictor",
        "@maven//:com_sun_istack_istack_commons_runtime",
        "@maven//:com_sun_jersey_contribs_jersey_apache_client4",
        "@maven//:com_sun_jersey_jersey_client",
        "@maven//:com_sun_jersey_jersey_core",
        "@maven//:com_sun_jersey_jersey_server",
        "@maven//:com_sun_jersey_jersey_servlet",
        "@maven//:com_thoughtworks_xstream_xstream",
        "@maven//:com_vaadin_external_google_android_json",
        "@maven//:commons_codec_commons_codec",
        "@maven//:commons_configuration_commons_configuration",
        "@maven//:commons_lang_commons_lang",
        "@maven//:io_github_x_stream_mxparser",
        "@maven//:io_micrometer_micrometer_core",
        "@maven//:io_micrometer_micrometer_registry_prometheus",
        "@maven//:io_projectreactor_addons_reactor_extra",
        "@maven//:io_projectreactor_reactor_core",
        "@maven//:io_prometheus_simpleclient",
        "@maven//:io_prometheus_simpleclient_common",
        "@maven//:io_prometheus_simpleclient_tracer_common",
        "@maven//:io_prometheus_simpleclient_tracer_otel",
        "@maven//:io_prometheus_simpleclient_tracer_otel_agent",
        "@maven//:jakarta_activation_jakarta_activation_api",
        "@maven//:jakarta_annotation_jakarta_annotation_api",
        "@maven//:jakarta_xml_bind_jakarta_xml_bind_api",
        "@maven//:javax_inject_javax_inject",
        "@maven//:javax_ws_rs_jsr311_api",
        "@maven//:junit_junit",
        "@maven//:net_bytebuddy_byte_buddy",
        "@maven//:net_bytebuddy_byte_buddy_agent",
        "@maven//:net_minidev_accessors_smart",
        "@maven//:net_minidev_json_smart",
        "@maven//:org_apache_httpcomponents_httpclient",
        "@maven//:org_apache_httpcomponents_httpcore",
        "@maven//:org_apache_logging_log4j_log4j_api",
        "@maven//:org_apache_logging_log4j_log4j_to_slf4j",
        "@maven//:org_apache_tomcat_embed_tomcat_embed_core",
        "@maven//:org_apache_tomcat_embed_tomcat_embed_el",
        "@maven//:org_apache_tomcat_embed_tomcat_embed_websocket",
        "@maven//:org_apiguardian_apiguardian_api",
        "@maven//:org_aspectj_aspectjweaver",
        "@maven//:org_assertj_assertj_core",
        "@maven//:org_codehaus_woodstox_stax2_api",
        "@maven//:org_freemarker_freemarker",
        "@maven//:org_glassfish_jaxb_jaxb_runtime",
        "@maven//:org_glassfish_jaxb_txw2",
        "@maven//:org_hamcrest_hamcrest",
        "@maven//:org_hamcrest_hamcrest_core",
        "@maven//:org_junit_jupiter_junit_jupiter",
        "@maven//:org_junit_jupiter_junit_jupiter_api",
        "@maven//:org_junit_jupiter_junit_jupiter_engine",
        "@maven//:org_junit_jupiter_junit_jupiter_params",
        "@maven//:org_junit_platform_junit_platform_commons",
        "@maven//:org_junit_platform_junit_platform_engine",
        "@maven//:org_mockito_mockito_core",
        "@maven//:org_mockito_mockito_junit_jupiter",
        "@maven//:org_objenesis_objenesis",
        "@maven//:org_opentest4j_opentest4j",
        "@maven//:org_ow2_asm_asm",
        "@maven//:org_projectlombok_lombok",
        "@maven//:org_reactivestreams_reactive_streams",
        "@maven//:org_skyscreamer_jsonassert",
        "@maven//:org_slf4j_jul_to_slf4j",
        "@maven//:org_slf4j_slf4j_api",
        "@maven//:org_springframework_boot_spring_boot",
        "@maven//:org_springframework_boot_spring_boot_actuator",
        "@maven//:org_springframework_boot_spring_boot_actuator_autoconfigure",
        "@maven//:org_springframework_boot_spring_boot_autoconfigure",
        "@maven//:org_springframework_boot_spring_boot_loader",
        "@maven//:org_springframework_boot_spring_boot_starter",
        "@maven//:org_springframework_boot_spring_boot_starter_actuator",
        "@maven//:org_springframework_boot_spring_boot_starter_aop",
        "@maven//:org_springframework_boot_spring_boot_starter_cache",
        "@maven//:org_springframework_boot_spring_boot_starter_freemarker",
        "@maven//:org_springframework_boot_spring_boot_starter_json",
        "@maven//:org_springframework_boot_spring_boot_starter_logging",
        "@maven//:org_springframework_boot_spring_boot_starter_test",
        "@maven//:org_springframework_boot_spring_boot_starter_tomcat",
        "@maven//:org_springframework_boot_spring_boot_starter_web",
        "@maven//:org_springframework_boot_spring_boot_test",
        "@maven//:org_springframework_boot_spring_boot_test_autoconfigure",
        "@maven//:org_springframework_cloud_spring_cloud_commons",
        "@maven//:org_springframework_cloud_spring_cloud_context",
        "@maven//:org_springframework_cloud_spring_cloud_loadbalancer",
        "@maven//:org_springframework_cloud_spring_cloud_netflix_eureka_client",
        "@maven//:org_springframework_cloud_spring_cloud_netflix_eureka_server",
        "@maven//:org_springframework_cloud_spring_cloud_starter",
        "@maven//:org_springframework_cloud_spring_cloud_starter_loadbalancer",
        "@maven//:org_springframework_cloud_spring_cloud_starter_netflix_eureka_server",
        "@maven//:org_springframework_security_spring_security_crypto",
        "@maven//:org_springframework_security_spring_security_rsa",
        "@maven//:org_springframework_spring_aop",
        "@maven//:org_springframework_spring_beans",
        "@maven//:org_springframework_spring_context",
        "@maven//:org_springframework_spring_context_support",
        "@maven//:org_springframework_spring_core",
        "@maven//:org_springframework_spring_expression",
        "@maven//:org_springframework_spring_jcl",
        "@maven//:org_springframework_spring_test",
        "@maven//:org_springframework_spring_web",
        "@maven//:org_springframework_spring_webmvc",
        "@maven//:org_xmlunit_xmlunit_core",
        "@maven//:org_yaml_snakeyaml",
        "@maven//:xmlpull_xmlpull",
        "@maven_service_registry//:org_bouncycastle_bcpkix_jdk15on",
        "@maven_service_registry//:org_bouncycastle_bcprov_jdk15on",
        "@maven_service_registry//:org_hdrhistogram_HdrHistogram",
    ],
)


# Build the app as a Spring Boot executable jar
springboot(
    name = "service-registry",
    boot_app_class="nl.robfaber.registry.ServiceRegistry",
    java_library = ":service-registry-lib",

    # DEPS ARE OPTIONAL HERE
    #  The springboot rule inherits all deps and runtime_deps from the java_library
    # deps = [],

    # TO TEST THE DUPE CLASSES FEATURE:
    #   There is an intentionally duplicated class in lib1 and lib2. Do this:
    #   1. set fail_on_duplicate_classes = True
    #   2. comment out lib1 or lib2 in demoapp_dupeclass_allowlist.txt
    #   Build should fail due to the duplicate class.
#    dupeclassescheck_enable = True,
#    dupeclassescheck_ignorelist = "demoapp_dupeclass_allowlist.txt",

    # Specify optional JVM args to use when the application is launched with 'bazel run'
#    bazelrun_jvm_flags = "-Dcustomprop=gold -DcustomProp2=silver",

    # data files can be made available in the working directory for when the app is launched with bazel run
#    bazelrun_data = ["example_data.txt"],

    # run the application in the background (command returns immediately)
    #bazelrun_background = True,

    # you may choose to override the launcher script that is used when you invoke 'bazel run //examples/demoapp'
#    bazelrun_script = "custom_bazelrun_script.sh",

    # if you have conflicting classes in dependency jar files, you can define the order in which the jars are loaded
    #  https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-executable-jar-format.html#executable-jar-war-index-files-classpath
#    deps_index_file = "demoapp_classpath.idx",
)
