package nl.robfaber.test.portfolio.types;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HelloResponse {
  private String value;
  private String name;
}
