package nl.robfaber.test.portfolio;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import nl.robfaber.test.portfolio.types.HelloResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@Slf4j
public class PortfolioStepDefinitions {
  private static final String URL = "http://localhost:9102";
  private final RestTemplate restTemplate = new RestTemplate();
  private ResponseEntity<HelloResponse> helloResult;

  @When("we call the hello endpoint")
  public void weCallTheHelloEndpoint() {
    try {
      helloResult = restTemplate.getForEntity(URL + "/hello", HelloResponse.class);
    } catch (Exception e) {
      log.warn("Call failed {}", e.getMessage());
    }
  }

  @Then("the response code is {int}")
  public void theReponseCode(int status) {
    assertNotNull(helloResult);
    assertEquals(helloResult.getStatusCodeValue(), status);
  }

  @Then("The response body is has value {string} and name {string}")
  public void theReponseBody(String value, String name) {
    val body = helloResult.getBody();
    assertEquals(body.getValue(), value);
    assertEquals(body.getName(), name);
  }
}
