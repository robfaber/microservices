Feature: Test portfolio service

  Scenario: Hello endpoint should respond
    When we call the hello endpoint
    Then the response code is 200
    Then The response body is has value "Hello" and name "key"
