package nl.robfaber.vizceral.model.traffic;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Metrics {
  @Builder.Default
  private Double normal = 0d;
  @Builder.Default
  private Double danger = 0d;


  public void add(Metrics metrics) {
    if (metrics == null) {
      return;
    }
    normal += metrics.getNormal();
    danger += metrics.getDanger();
  }
}
