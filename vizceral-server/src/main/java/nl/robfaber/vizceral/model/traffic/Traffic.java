package nl.robfaber.vizceral.model.traffic;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Traffic {

  @Builder.Default
  private String renderer = "global";
  @Builder.Default
  private String name = "edge";
  private List<Node> nodes;
  private List<Connection> connections;
  private long serverUpdateTime;
  @Builder.Default
  private Long maxVolume = 5000L;
}

