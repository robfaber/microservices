package nl.robfaber.vizceral.model.discovery;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class Application {
  private String name;
  private List<Instance> instance;
}
