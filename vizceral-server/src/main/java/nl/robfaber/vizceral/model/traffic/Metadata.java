package nl.robfaber.vizceral.model.traffic;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Metadata {
  private String name;
  private Integer streaming;
}
