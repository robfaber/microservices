package nl.robfaber.vizceral.model.traffic;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Connection {
  private String source;
  private String target;
  private Metrics metrics;
}
