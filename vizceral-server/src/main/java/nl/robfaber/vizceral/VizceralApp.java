package nl.robfaber.vizceral;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VizceralApp {
  public static void main(String args[]) {
    SpringApplication.run(VizceralApp.class, args);
  }
}
