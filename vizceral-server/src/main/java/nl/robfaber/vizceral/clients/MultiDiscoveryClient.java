package nl.robfaber.vizceral.clients;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.robfaber.vizceral.config.VizceralProperties;
import nl.robfaber.vizceral.model.discovery.Application;
import nl.robfaber.vizceral.model.discovery.ApplicationsResult;
import nl.robfaber.vizceral.model.discovery.Instance;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * This client retrieves instances of microservices from multiple (unlinked service-registries)
 * In this way we can collect the instances from multiple datacenters while normally we don't allow
 * for cross datacenter traffic.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class MultiDiscoveryClient {

  private final VizceralProperties properties;
  private final RestClient restClient;

  private AtomicBoolean runningUpdate = new AtomicBoolean(false);

  private Map<String, List<Instance>> instancesPerDataCenter;
  private Map<String, Set<String>> applicationsPerDataCenter;
  private Set<String> services;
  private Set<String> dataCenters;

  public Set<String> getServices() {
    return services;
  }

  public Set<String> getDataCenters() {
    return dataCenters;
  }

  public List<Instance> getInstances(String dataCenter) {
    return instancesPerDataCenter.get(dataCenter);
  }

  public Set<String> getApplications(String dataCenter) {
    return applicationsPerDataCenter.get(dataCenter);
  }

  public void refresh() {
    if (runningUpdate.getAndSet(true)) {
      log.debug("Refresh already running not starting another");
      return;
    }
    try {
      services = new HashSet<>();
      dataCenters = new HashSet<>();
      instancesPerDataCenter = new HashMap<>();
      applicationsPerDataCenter = new HashMap<>();

      for (String eurekaUrl : properties.getEurekaUrls()) {
        List<Application> applications = retrieveApplications(eurekaUrl);

        applications.forEach(application -> {
          String service = application.getName().toLowerCase();
          services.add(service);

          application.getInstance().forEach(instance -> {
            String dataCenter = instance.getDataCenter();
            dataCenters.add(dataCenter);

            Set<String> services = applicationsPerDataCenter.computeIfAbsent(dataCenter, k -> new HashSet<>());
            services.add(service);

            List<Instance> instances = instancesPerDataCenter.computeIfAbsent(dataCenter, k -> new ArrayList<>());
            instances.add(instance);
          });
        });
      }
    } finally {
      runningUpdate.set(false);
    }
  }

  private List<Application> retrieveApplications(String eurekaUrl) {
    try {
      return restClient.executeRequest("service-registry", eurekaUrl + "/apps", ApplicationsResult.class).getApplications().getApplication();
    } catch (Exception e) {
      log.warn("Eureka with url {} is failing our applications request. Is it UP?", eurekaUrl);
      return Collections.emptyList();
    }
  }
}
