package nl.robfaber.vizceral.clients;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.robfaber.vizceral.model.discovery.Instance;
import nl.robfaber.vizceral.model.requests.AvailableTags;
import nl.robfaber.vizceral.model.requests.MetricNames;
import nl.robfaber.vizceral.model.requests.RequestMetrics;
import nl.robfaber.vizceral.model.traffic.Connection;
import nl.robfaber.vizceral.model.traffic.Metrics;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static nl.rofaber.metrics.RequestMetricsService.CLIENT_ERROR;
import static nl.rofaber.metrics.RequestMetricsService.INCOMING_REQUESTS;
import static nl.rofaber.metrics.RequestMetricsService.OUTCOME_CLIENT_ERROR;
import static nl.rofaber.metrics.RequestMetricsService.OUTCOME_SERVER_ERROR;
import static nl.rofaber.metrics.RequestMetricsService.OUTCOME_SUCCESS;
import static nl.rofaber.metrics.RequestMetricsService.OUTGOING_REQUESTS;
import static nl.rofaber.metrics.RequestMetricsService.SERVER_ERROR;
import static nl.rofaber.metrics.RequestMetricsService.SERVICE_ID_INTERNET;
import static nl.rofaber.metrics.RequestMetricsService.SUCCESS;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings("PMD.TooManyStaticImports")
public class RequestMetricsClient {

  private final RestClient restRequestHandler;

  public List<Connection> getOutgoingRequestMetrics(Instance instance) {
    List<Connection> outgoingConnections = new ArrayList<>();

    try {
      //Fetch the top level metric to get the serviceIds see for example http://localhost:8090/actuator/metrics/outgoing.requests
      String url = getActuatorMetricsUrl(instance);
      MetricNames metricNames = restRequestHandler.executeRequest(instance.getApp(), url, MetricNames.class);
      if (metricNames == null || !metricNames.contains(OUTGOING_REQUESTS)) {
        log.debug("Service {} does not support '{}' metrics", instance.getApp(), OUTGOING_REQUESTS);
        //If instance does not expose outgoing.requests metric return empty list
        return outgoingConnections;
      }

      //The instance has outgoing.http.requests metric
      final String outgoingUrl = getActuatorMetricsUrl(instance, OUTGOING_REQUESTS);
      RequestMetrics topLevelMetrics = restRequestHandler.executeRequest(instance.getApp(), outgoingUrl, RequestMetrics.class);
      Optional<AvailableTags> serviceIdTags = topLevelMetrics.getAvailableTags().stream()
          .filter(tags -> tags.getTag().equalsIgnoreCase("serviceId"))
          .findFirst();
      Optional<AvailableTags> outcomeTags = topLevelMetrics.getAvailableTags().stream()
          .filter(tags -> tags.getTag().equalsIgnoreCase("outcome"))
          .findFirst();

      if (serviceIdTags.isPresent() && outcomeTags.isPresent()) {
        List<String> serviceIds = serviceIdTags.get().getValues();

        serviceIds.forEach(serviceId -> {

          Double danger = 0d;
          Double normal = 0d;

          //Get the RequestMetrics per ServiceId
          RequestMetrics metrics;
          if (outcomeTags.get().getValues().contains(CLIENT_ERROR)) {
            metrics = getMetrics(instance, outgoingUrl, Tags.of(
                OUTCOME_CLIENT_ERROR,
                Tag.of("serviceId", serviceId)
            ));
            danger += metrics.getCount().getValue();
          }

          if (outcomeTags.get().getValues().contains(SERVER_ERROR)) {
            metrics = getMetrics(instance, outgoingUrl, Tags.of(
                OUTCOME_SERVER_ERROR,
                Tag.of("serviceId", serviceId)
            ));
            danger += metrics.getCount().getValue();
          }

          if (outcomeTags.get().getValues().contains(SUCCESS)) {
            metrics = getMetrics(instance, outgoingUrl, Tags.of(
                OUTCOME_SUCCESS,
                Tag.of("serviceId", serviceId)
            ));
            normal += metrics.getCount().getValue();
          }

          outgoingConnections.add(Connection.builder()
              .source(instance.getApp().toLowerCase())
              .target(serviceId.toLowerCase())
              .metrics(new Metrics(normal, danger))
              .build());
        });
      }
    } catch (Exception e) {
      log.warn("Failed to retrieve outgoing connections for {} with exception", instance.getApp(), e.getMessage());
    }
    return outgoingConnections;
  }

  public Metrics getIncomingRequestMetrics(Instance instance) {

    //Fetch the top level metric to get the serviceIds see for example http://localhost:8090/actuator/metrics/incoming.requests
    String url = getActuatorMetricsUrl(instance);
    MetricNames metricNames = restRequestHandler.executeRequest(instance.getApp(), url, MetricNames.class);
    if (!metricNames.contains(INCOMING_REQUESTS)) {
      log.debug("Service {} does not support '{}' metrics", instance.getApp(), INCOMING_REQUESTS);
      //If instance does not expose incoming.requests metric return empty
      return new Metrics(0d, 0d);
    }

    final String baseMetricsUrl = getActuatorMetricsUrl(instance, INCOMING_REQUESTS);
    RequestMetrics topLevelMetrics = restRequestHandler.executeRequest(instance.getApp(), baseMetricsUrl, RequestMetrics.class);
    Optional<AvailableTags> outcomeTags = topLevelMetrics.getAvailableTags().stream()
        .filter(tags -> tags.getTag().equalsIgnoreCase("outcome"))
        .findFirst();

    Double danger = 0d;
    Double normal = 0d;
    if (outcomeTags.isPresent()) {
      RequestMetrics metrics;
      if (outcomeTags.get().getValues().contains(CLIENT_ERROR)) {
        metrics = getMetrics(instance, baseMetricsUrl, Tags.of(OUTCOME_CLIENT_ERROR, SERVICE_ID_INTERNET));
        danger += metrics.getCount().getValue();
      }

      if (outcomeTags.get().getValues().contains(SERVER_ERROR)) {
        metrics = getMetrics(instance, baseMetricsUrl, Tags.of(OUTCOME_SERVER_ERROR, SERVICE_ID_INTERNET));
        danger += metrics.getCount().getValue();
      }

      if (outcomeTags.get().getValues().contains(SUCCESS)) {
        metrics = getMetrics(instance, baseMetricsUrl, Tags.of(OUTCOME_SUCCESS, SERVICE_ID_INTERNET));
        normal += metrics.getCount().getValue();
      }
    }
    return new Metrics(normal, danger);
  }


  private RequestMetrics getMetrics(Instance instance, String baseMetricsUrl, Tags tags) {
    StringBuilder url = new StringBuilder(baseMetricsUrl);
    boolean first = true;
    for (Tag tag : tags) {
      if (first) {
        url.append("?tag=");
      } else {
        url.append("&tag=");
      }
      first = false;
      url.append(tag.getKey()).append(":").append(tag.getValue());
    }

    RequestMetrics metrics = restRequestHandler.executeRequest(instance.getApp(), url.toString(), RequestMetrics.class);
    if (metrics == null) {
      //We also get a 404 from the metric if there is no value for it
      return RequestMetrics.builder().build();
    }
    return metrics;
  }

  private String getActuatorMetricsUrl(Instance instance) {
    return instance.getHealthCheckUrl().replace("health", "metrics");
  }

  private String getActuatorMetricsUrl(Instance instance, String metricName) {
    return instance.getHealthCheckUrl().replace("health", "metrics") + "/" + metricName;
  }
}
