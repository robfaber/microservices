package nl.robfaber.vizceral.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "vizceral")
@Data
public class VizceralProperties {
  // Edge services are entry points into a services landscape like api-gateway or internal-gateway
  private List<String> edgeServices;
  // Eureka urls from different data centers
  private List<String> eurekaUrls;
  // Extra nodes not from eureka
  private List<String> extraNodes;
  //Services to include
  private List<String> includedServices;
}
