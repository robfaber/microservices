package nl.robfaber.vizceral.endpoints;

import lombok.RequiredArgsConstructor;
import nl.robfaber.vizceral.model.traffic.Traffic;
import nl.robfaber.vizceral.services.TrafficService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/vizceral/api/traffic")
@CrossOrigin(origins = {"http://localhost:8080", "http://0.0.0.0:8080", "http://localhost:3000"})
@RequiredArgsConstructor
public class TrafficEndpoint {

  private final TrafficService trafficService;

  @GetMapping
  public Traffic getTraffic() {
    return trafficService.getTraffic();
  }
}