package nl.robfaber.example.experience.services;

import io.micrometer.core.annotation.Timed;
import nl.robfaber.example.experience.config.FeignConfiguration;
import nl.robfaber.quote.model.Quote;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "quote-service", configuration = FeignConfiguration.class)
@Timed
public interface QuoteServiceClient {

  @GetMapping(value = "/quotes")
  @Timed(value = "feign.quotes")
  List<Quote> getQuotes();

  @GetMapping(value = "/quotes/{symbol}")
  @Timed(value = "feign.quote")
  Quote getQuote(@PathVariable("symbol") String symbol);
}
