package nl.robfaber.example.experience.dto;

import lombok.Data;

@Data
public class HelloResponse {
  private String value;
}
