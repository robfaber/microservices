package nl.robfaber.example.experience.services;

import io.micrometer.core.annotation.Timed;
import nl.robfaber.example.experience.config.FeignConfiguration;
import nl.robfaber.example.experience.dto.HelloResponse;
import nl.robfaber.example.experience.dto.Instrument;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(value = "portfolio-service", configuration = FeignConfiguration.class)
@Timed
public interface CapabilityClient {

  @GetMapping(value = "/hello", consumes = "application/json")
  @Timed(value = "feign.hello")
  HelloResponse hello();

  @RequestMapping(method = RequestMethod.GET, value = "/instruments", consumes = "application/json")
  @Timed(value = "feign.instruments")
  List<Instrument> getInstruments();
}
